import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Habit from './Habit';

// TODO: Change habitRow to use flex
const HabitsListWrapper = styled.div`
  display: grid;
  grid-template-columns: 250px repeat(31, minmax(10px, 30px)) 1fr;
  grid-auto-rows: minmax(30px, auto);
  grid-gap: 3px;
  align-content: center;
  margin: 20px 3em;
  background-color: ${props => props.theme.colors.silver};
`;


// TODO: put `habitsData` in Redux Store Initial state
const Habits = ({ habitsData }) => {
  // TODO: Dynamically Choose the month not [0]
  // TODO: turn habitRow into it's own component
  const habitRow = habitsData.map(habit =>
    (<Habit
      key={habit.habitName.toString()}
      habitName={habit.habitName}
      monthData={habit.months[0]}
    />));

  return (
    <HabitsListWrapper>
      {habitRow}
    </HabitsListWrapper>
  );
};

Habits.propTypes = {
  habitsData: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
};

export default Habits;
