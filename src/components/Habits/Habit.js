import React, { Fragment } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const HabitName = styled.div`
  background-color: ${props => props.theme.colors.yellow};
  grid-column-start: 1;
  text-align: left;
  padding: 5px 10px;
`;

const HabitBox = styled.div`
  display:grid;
  grid-template-columns: 1fr;
  grid-template-rows: 100%;
  align-items: center;
  text-align: center;
  background-color: ${props => props.theme.colors.gray};
  border-radius: 4px;
  cursor: pointer;

  :hover {
    background-color: ${props => props.theme.colors.green};
  }
`;

const Habit = ({ habitName, monthData }) => {
  // TODO: Dynamically add class based on 'completed' bool in 'day'
  const habitBox = monthData.days.map(dayObj =>
    <HabitBox key={dayObj.day.toString()}>{dayObj.day}</HabitBox>);

  return (
    <Fragment>
      <HabitName>{habitName}</HabitName>
      {habitBox}
    </Fragment>
  );
};

Habit.propTypes = {
  habitName: PropTypes.string.isRequired,
  monthData: PropTypes.shape().isRequired,
};

export default Habit;
