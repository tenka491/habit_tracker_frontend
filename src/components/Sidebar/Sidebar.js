// TODO: Add Navigation elements

import React, { PureComponent } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { navigateToOverview } from './actions';

const OuterWrapper = styled.div`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  padding: 30px 15px;
  width: 100%;
  height: 100%;
`;

const NavigationLink = styled.div`
  width: 100%;
  cursor: pointer;
  color: ${props => props.theme.colors.darkGreen};
  text-decoration: none;

  :hover {
    color: ${props => props.theme.colors.green};
  }
`;

class Sidebar extends PureComponent {
  render() {
    return (
      <OuterWrapper>
        <h3>something</h3>
        <div>
          <NavigationLink
            onClick={(e) => {
              e.preventDefault();
              console.log('link clicked', this.props);
              this.props.dispatch(navigateToOverview());
            }
          }
          >
            Overview
          </NavigationLink>
        </div>
        <div>Manage Habits</div>
      </OuterWrapper>
    );
  }
}

Sidebar.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
// TODO: don't need to the spread operator vvv
const mapStateToProps = state => ({ ...state });

export default connect(mapStateToProps)(Sidebar);
