import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledLink = styled.a`
  color: ${props => props.theme.colors.darkGreen};
  text-decoration: none;

  :hover {
    color: ${props => props.theme.colors.green};
  }
`;

const Link = ({ children, href }) => (
  <StyledLink href={href}>{children}</StyledLink>
);

Link.propTypes = {
  children: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
};

export default Link;
