const initialState = {
  currentPage: 'other',
};

function pageState(state = initialState, action) {
  switch (action.type) {
    case 'NAVIGATE_TO_OVERVIEW':
      console.log('pageState overview');
      return { ...state, currentPage: 'overview' };

    default:
      return state;
  }
}

export default pageState;
