const defaultTheme = {
  colors: {
    yellow: "#FFED90",
    green: "#A8D46F",
    darkGreen: "#359668",
    purple: "#3C3251",
    darkPurple: "#341139",
    white: "#F2F2F2",
    black: "#111111",
    gray: "#AAAAAA",
    silver: "#DDDDDD"
  },
  text: {
    color: "#333333",
    size: "16px",
  },
};

export default defaultTheme;
