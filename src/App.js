import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

import moment from 'moment';

import theme from './themes/defaultTheme';
import data from './data.json';

import Sidebar from './components/Sidebar';
import Habits from './components/Habits';

const AppWrapper = styled.div`
  position: absolute;
  display: grid;
  grid-template-areas: 
    "header header"
    "sidebar workspace";
  grid-template-columns: 200px auto;
  grid-template-rows: 100px auto;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  color: ${props => props.theme.text.color};
  font-size: ${props => props.theme.text.size};
`;

const HeaderWrapper = styled.div`
  grid-area: header;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.colors.purple};
  color: ${props => props.theme.colors.silver};
  text-transform: uppercase;
`;

const SidebarWrapper = styled.div`
  grid-area: sidebar;
  background-color: ${props => props.theme.colors.silver};
  border-right: 1px solid ${props => props.theme.colors.gray};
`;

const WorkspaceWrapper = styled.div`
  grid-area: workspace;
  background-color: ${props => props.theme.colors.white};
`;

const App = () => {
  const currentMonth = moment().format('MMMM');

  return (
    <ThemeProvider theme={theme}>
      <AppWrapper>
        <HeaderWrapper>
          <h1>Track your {currentMonth} Habits</h1>
        </HeaderWrapper>

        <SidebarWrapper>
          <Sidebar />
        </SidebarWrapper>

        <WorkspaceWrapper>
          <Habits habitsData={data} />
        </WorkspaceWrapper>

      </AppWrapper>
    </ThemeProvider>
  );
};

export default App;
